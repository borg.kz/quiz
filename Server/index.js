var express = require('express');
var app = express();
var Storage = require('map-cache');
var session = require('express-session')
var fs = require('fs');
var cors = require('cors');

var questionsBank = JSON.parse(fs.readFileSync('QuestionsBank.json', 'utf8'));
var storage = new Storage();

// Server port
var HTTP_PORT = 3000

app.use(cors());

app.use(session({
  secret: 'abracaabra',
  resave: false,
  saveUninitialized: true
}))

app.use(function (req, res, next) {
	if (!req.session.views) {
	  req.session.views = {}
	}

	next()
})

// Root endpoint
app.get("/", (req, res, next) => {
	if (!storage.has('key'))
		storage.set('key', 0);

	var value = storage.get('key');
	storage.set('key', ++value);
	res.cookie('OmicronQuiz', 'req.query.userName');
    res.json({"message":"Ok " + value, "view": req.session.views["qwer"] + ' times'});
});

app.get('/login', function (req, res) {
	var users = app.get("Users");
	var results = null;
	if (users == null)
		users = {};

	if (users[req.query.userName] == null) {
		results = new Object();
		results.points = 0.0;
		results.question = 0;
		results.isComplete = false;
		results.userName = req.query.userName;
	}

	users[req.query.userName] = results;
	app.set("Users", users);

	res.json({"message":"Ok"});
});

app.get("/quiz", (req, res, next) => {
	var userName = req.query.userName;
	let users = app.get("Users");
	let results = users[userName];
	let currentQuestion = questionsBank[results.question];
	let sorting = Object.keys(users).map(function (key) {
		return { key: users[key].points, value: key };
	});

	sorting.sort(function (a, b) {
		return (a.value > b.value) ? -1 : 1;
	});

	let place = 0;
	for (let i = 0; i < sorting.length; i++) {
		if (sorting[i].value == userName) {
			place = i + 1;
			break;
		}
	}

	res.json({
		"Place": place,
		"Task": "",
		"Number": results.question + 1,
		"Text" : currentQuestion.Question,
		"A": currentQuestion.Answers.A,
		"B": currentQuestion.Answers.B,
		"C": currentQuestion.Answers.C,
		"D": currentQuestion.Answers.D
	});
});

app.get("/results", (req, res, next) => {
	let users = app.get("Users");
	let result = Object.keys(users).map(function (key) {
		return users[key];
	});

	res.json(JSON.stringify(result));
});

app.get("/save", (req, res, next) => {
	var points = parseInt(req.query.points);
	var userName = req.query.userName;

	if (points === null)
		points = 0;

	let results = app.get("Users")[userName];
	results.points += points !== null ? points : 0;

	results.question++;

	if (results.question > questionsBank.length - 1) {
		results.isComplete = true;
		res.json({
			"Complition":"true"
		});
		return;
	}

	res.json({
		"Complition":"false"
	});
});

app.listen(HTTP_PORT, function () {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});