import { Component, OnInit } from '@angular/core';
import { BookStoreService } from '../book-store.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
})

export class ResultsComponent implements OnInit {
  results : [];

  constructor(private httpService: BookStoreService) { }

  ngOnInit() {
    this.httpService.getResults().subscribe(data => {
        let json = JSON.parse(data);
        this.results = json;
        this.results.forEach(element => {
          element.current = element["userName"] == localStorage.getItem('token');
        });

        this.results.sort(function (a, b) {
          return (a["points"] > b["points"]) ? -1 : 1;
        });
    });

    setInterval(function() {
      window.location.reload();
    }, 12000)
  }
}
