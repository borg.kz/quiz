import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-clear',
  templateUrl: './clear.component.html'
})
export class ClearComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    localStorage.removeItem('token');
    localStorage.removeItem('completed');
    this.router.navigateByUrl('/');
  }
}
