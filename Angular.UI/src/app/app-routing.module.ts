import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { AuthGuard } from './auth/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { QuizComponent } from './quiz/quiz.component';
import { ResultsComponent } from './results/results.component';
import { ClearComponent } from './clear/clear.component';


const routes: Routes = [
  { path:'', redirectTo:'/user/login', pathMatch:'full' },
  {
    path: 'user', component: UserComponent,
    children: [
      { path: 'registration', component: RegistrationComponent },
      { path: 'login', component: LoginComponent }
    ]
  },
//  {path:'home',component:HomeComponent},
  { path:'home', component: HomeComponent },
  { path:'quiz', component: QuizComponent },
  { path:'results', component: ResultsComponent },
  { path:'clear', component: ClearComponent },
  { path:'forbidden', component: ForbiddenComponent },
  { path:'adminpanel', component: AdminPanelComponent, canActivate: [AuthGuard], data: {permittedRoles:['Admin']} }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
