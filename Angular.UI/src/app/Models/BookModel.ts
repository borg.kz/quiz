export class BookModel {
    Uid: string;
    Title: string;
    Description: string;
    RentRule: string;
    Publisher: string;
    PublishDate: string;
    Isbn: string;
    Lui: string;
    Reason: string;
    Quantity: string;
}

export class DataModel {
    constructor() {
        // TODO Arch#1: To create proxy structure of data from WebApi, we can be generated whole structure thru PostBuild action in visual studio
        // var st: string = DataModel.MBook.Title;
        // Add the same we can implement for calling of controller/Action
    }
    
    public static readonly MBook : MBook;
}

export class MBook {
    public static readonly Title = "Title";
    private static readonly Isbh = "Isbn";
}

export class TestItem {
    Place: string;
    Task: string;
    Number: string;
    Text: string;
    A: string;
    B: string;
    C: string;
    D: string;
}

export class AnswerStatus {
    Complition: string;
}