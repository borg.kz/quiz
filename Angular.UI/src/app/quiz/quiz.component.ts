import { Component, OnInit } from '@angular/core';
import { BookStoreService } from '../book-store.service';
import { TestItem } from '../Models/BookModel';
import { Router } from "@angular/router";

var t1 = null;

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html'
})

export class QuizComponent implements OnInit {
  testItem: TestItem;

  constructor(private httpService: BookStoreService, private router: Router) { }

  ngOnInit() {
      if (localStorage.getItem('completed') === "true") {
        this.router.navigateByUrl('/results');
        return;
      }

      this.getTestitem();
      t1 = performance.now();
  }

  getTestitem(): void {
    this.httpService.getQuestion(localStorage.getItem('token')).subscribe(data => {
      this.testItem = data;
    });

    QuizComponent.$timeout(this.httpService);
  }

  static $timeout(service) {
    setInterval(function() {
      service.saveAnswer(0, localStorage.getItem('token')).subscribe(data => {
        if (data.Complition === 'true') {
            localStorage.setItem('completed', "true");
            this.router.navigateByUrl('/results');
            return;
        }
      });
      window.location.reload();
    }, 915100);
  }

  public Answer(letter: number) {
    let points = 15000 - performance.now() - t1;
    let answer = parseInt(this.testItem.Number) % 4;
    if (answer == 0)
        answer = 4;
    if (letter != answer)
        points = 0;

    alert("Points" + points);
    this.httpService.saveAnswer(Math.floor(points), localStorage.getItem('token')).subscribe(data => {
      if (data.Complition === 'true') {
          localStorage.setItem('completed', "true");
          this.router.navigateByUrl('/results');
          return;
      }
    });
    window.location.reload();
  }
}
